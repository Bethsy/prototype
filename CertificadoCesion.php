<?php
/**
 Implementacion corta de Patron de Diseño
 */

namespace Prototype;

require_once "Documento.php";
require_once "../../Herramientas.php";

class CertificadoCesion extends Documento
{
    
    public function muestra()
    {
        \Herramientas::println("Muestra el certificado de cesión: $this->contenido");
    }

    public function imprime()
    {
        \Herramientas::println("Imprime el certificado de cesión: $this->contenido");
    }
}

