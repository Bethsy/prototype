<?php
/**
  Implementacion corta de Patron de Diseño
 */

namespace Prototype;


abstract class Documentacion implements \IteratorAggregate
{
    /**
     *
     * @var \ArrayObject
     */
    protected $documentos;

    /**
     * @return \ArrayIterator
     */
    public function getIterator () {
        return $this->documentos->getIterator();
    }
}