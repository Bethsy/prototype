<?php
/**
 Implementacion corta de Patron de Diseño
 */

namespace Prototype;

require_once "Documento.php";
require_once "../../Herramientas.php";

class SolicitudMatriculacion extends Documento
{
    public function muestra()
    {
        \Herramientas::println("Muestra la solicitud de matriculación: $this->contenido");
    }

    public function imprime()
    {
        \Herramientas::println("Imprime la solicitud de matriculación : $this->contenido");
    }
}